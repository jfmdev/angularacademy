import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { UnsafeAction } from './unsafeAction';

@Injectable()
export class SessionActions {
  static LOGOUT = '[Session] Logout';
  logout(): Action {
    return {
      type: SessionActions.LOGOUT
    };
  }

  static LOGIN = '[Session] Login';
  login(username, password): UnsafeAction {
    return {
      type: SessionActions.LOGIN,
      payload: {'username': username, 'password': password}
    };
  }

  static LOGIN_SUCCESS = '[Session] Login Success';
  loginSuccess(user): UnsafeAction {
    return {
      type: SessionActions.LOGIN_SUCCESS,
      payload: user
    };
  }

  static LOGIN_ERROR = '[Session] Login Error';
  loginError(errorMsj): UnsafeAction {
    return {
      type: SessionActions.LOGIN_ERROR,
      payload: errorMsj
    };
  }

  static UPDATE_PROFILE = '[Session] Update Profile';
  updateProfile(user): UnsafeAction {
    return {
      type: SessionActions.UPDATE_PROFILE,
      payload: user
    };
  }

  static UPDATE_PROFILE_SUCCESS = '[Session] Update Profile Success';
  updateProfileSuccess(user): UnsafeAction {
    return {
      type: SessionActions.UPDATE_PROFILE_SUCCESS,
      payload: user
    };
  }
}