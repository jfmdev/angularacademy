import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { User } from '../models/user';
import { UnsafeAction } from './unsafeAction';

@Injectable()
export class UserActions {
  static RESET_BLANK_USER = '[User] Reset Blank User';
  resetBlankUser(): Action {
    return {
      type: UserActions.RESET_BLANK_USER
    };
  }

  static LOAD_USERS = '[User] Load Users';
  loadUsers(): Action {
    return {
      type: UserActions.LOAD_USERS
    };
  }

  static LOAD_USERS_SUCCESS = '[User] Load Users Success';
  loadUsersSuccess(users): UnsafeAction {
    return {
      type: UserActions.LOAD_USERS_SUCCESS,
      payload: users
    };
  }

  static GET_USER = '[User] Get User';
  getUser(id): UnsafeAction {
    return {
      type: UserActions.GET_USER,
      payload: id
    };
  }

  static GET_USER_SUCCESS = '[User] Get User Success';
  getUserSuccess(user): UnsafeAction {
    return {
      type: UserActions.GET_USER_SUCCESS,
      payload: user
    };
  }

  static SAVE_USER = '[User] Save User';
  saveUser(user): UnsafeAction {
    return {
      type: UserActions.SAVE_USER,
      payload: user
    };
  }

  static SAVE_USER_SUCCESS = '[User] Save User Success';
  saveUserSuccess(user): UnsafeAction {
    return {
      type: UserActions.SAVE_USER_SUCCESS,
      payload: user
    };
  }

  static ADD_USER = '[User] Add User';
  addUser(user): UnsafeAction {
    return {
      type: UserActions.ADD_USER,
      payload: user
    };
  }

  static ADD_USER_SUCCESS = '[User] Add User Success';
  addUserSuccess(user): UnsafeAction {
    return {
      type: UserActions.ADD_USER_SUCCESS,
      payload: user
    };
  }

  static DELETE_USER = '[User] Delete User';
  deleteUser(user): UnsafeAction {
    return {
      type: UserActions.DELETE_USER,
      payload: user
    };
  }

  static DELETE_USER_SUCCESS = '[User] Delete User Success';
  deleteUserSuccess(user): UnsafeAction {
    return {
      type: UserActions.DELETE_USER_SUCCESS,
      payload: user
    };
  }
}