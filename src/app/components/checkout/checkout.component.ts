import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';

import { ActiveUserService } from '../../services/active-user';
import { AppState } from '../../reducers/app-state'

import { Order } from '../../models/order';
import { OrderLine } from '../../models/order-line';
import { OrderActions } from '../../actions/order';
import { OrderState } from '../../reducers/order';
import { Product } from '../../models/product';
import { ProductService } from '../../services/product';
import { User } from '../../models/user';
import { UserService } from '../../services/user';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html'
})

export class CheckoutComponent implements OnInit, OnDestroy  {
  orderSubscription: any;
  order: Order;
  lines: any[];
  user: User;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private orderActions: OrderActions,
    private activeUser: ActiveUserService,
    private userSvc: UserService,
    private productSvc: ProductService,
    ) { 
  }

  ngOnInit() {
    // Subscribe for changes on the order state.
    let orderObservable: Observable<OrderState> = this.store.select('order');
    this.orderSubscription = orderObservable.subscribe(
      (next) => { 
        this.order = next.order;

        // Check the status is 'created' (otherwise redirect).
        if(!this.order.status || this.order.status == 'created') {
            // HACK: Add products data to each line (this should normally be done by the backend).
            this.lines = _.cloneDeep(this.order.lines) || [];
            this.productSvc.getProducts().subscribe(products => {
              for(let i=0; i<this.lines.length; i++) {
                this.lines[i].product = _.find(products, product => this.lines[i].productId == product.id);
              }
            }); 
            
            // Load user data.
            this.userSvc.getUser(this.order.userId).subscribe(user => this.user = user); 
        } else {
          this.router.navigate(['orders']);
        }
      }
    );

    // Load the order according to the id parameter.
    this.route.params.subscribe(params => {
      this.store.dispatch(this.orderActions.getOrder(params.orderId));
    });
  }

  ngOnDestroy() {
    this.orderSubscription.unsubscribe();
  }
  
  getTotalPrice(lines: any[]): number {
    let res: number = 0;
    if(lines) {
      for(let i=0; i<lines.length; i++) {
        res += lines[i].product? (lines[i].product.price * lines[i].quantity) : 0;
      }
    }
    return res;
  }
  
  payNow(order: Order) {
    // TODO: Pay using either the Paypal or Stripe sandboxs.
    order.status = 'payed';
    this.store.dispatch(this.orderActions.saveOrder(order));
  }
}