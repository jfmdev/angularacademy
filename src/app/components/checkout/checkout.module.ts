import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { CheckoutComponent } from './checkout.component';
import { routing } from './checkout.routing';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      routing
  ],
  declarations: [CheckoutComponent]
})
export class CheckoutModule {}