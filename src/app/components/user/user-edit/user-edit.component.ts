import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { User } from '../../../models/user';
import { Role } from '../../../models/role';
import { UserActions } from '../../../actions/user';
import { AppState } from '../../../reducers/app-state'
import { UserState } from '../../../reducers/user';
import { RoleService } from '../../../services/role';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html'
})

export class UserEditComponent implements OnInit, OnDestroy  {
  userSubscription: any;
  user: User;
  roleList: Role[];
  loading: boolean;
  saving: boolean;

  constructor(
    private store: Store<AppState>,
    private userActions: UserActions,
    private route: ActivatedRoute,
    private router: Router,
    private roles: RoleService
    ) { 
  }

  ngOnInit() {
    // Subscribe for changes on the user state.
    let userObservable: Observable<UserState> = this.store.select('user');
    this.userSubscription = userObservable.subscribe(
      (next) => { 
        this.user = next.user;
        this.user.roles = this.user.roles || [];
        this.loading = next.loading;
        this.saving = next.saving;
      }
    );

    // Load the user according to the id parameter.
    this.route.params.subscribe(params => {
      if(params.id) {
        this.store.dispatch(this.userActions.getUser(params.id));
      } else {
        this.store.dispatch(this.userActions.resetBlankUser());
      }
    });
    
    this.roleList = this.roles.getList();
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  save(form: any, user: User) {
    this.store.dispatch(this.userActions.saveUser(user));
  }

  delete(user: User) {
    if(confirm("Are you sure you want to delete this user?")) {
      this.store.dispatch(this.userActions.deleteUser(user));
      this.router.navigate(['user/list']);
    }
  }
  
  toggleRole(user: User, role: Role) {
    let index = user.roles.indexOf(role.key);
    if(index < 0) {
      user.roles.push(role.key);
    } else {
      user.roles.splice(index, 1);
    }
  }
}