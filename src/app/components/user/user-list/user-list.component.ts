import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { User } from '../../../models/user';
import { UserActions } from '../../../actions/user';
import { AppState } from '../../../reducers/app-state'
import { UserListState } from '../../../reducers/user-list';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})

export class UserListComponent implements OnInit {
  users: Observable<UserListState>;

  constructor(
    private store: Store<AppState>,
    private userActions: UserActions
    ) { 
  }

  ngOnInit() {
    // Load list of users.
    this.users = this.store.select('userList');
    this.store.dispatch(this.userActions.loadUsers());
  }
  
  delete(user: User) {
    if(confirm("Are you sure you want to delete user " + user.username)) {
      this.store.dispatch(this.userActions.deleteUser(user));
    }
    return false;
  }
}