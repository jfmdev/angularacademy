import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserEditComponent } from './user-edit/user-edit.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  { path: 'list', component: UserListComponent },
  { path: 'edit', component: UserEditComponent },
  { path: 'edit/:id', component: UserEditComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);