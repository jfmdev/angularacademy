import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { UserEditComponent } from './user-edit/user-edit.component';
import { UserListComponent } from './user-list/user-list.component';
import { routing } from './user.routing';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      routing
  ],
  declarations: [
    UserEditComponent,
    UserListComponent
  ]
})
export class UserModule {}