import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { Product } from '../../models/product';
import { ProductActions } from '../../actions/product';
import { AppState } from '../../reducers/app-state'
import { ProductState } from '../../reducers/product';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html'
})

export class ProductEditComponent implements OnInit, OnDestroy  {
  productSubscription: any;
  product: Product;
  loading: boolean;
  saving: boolean;

  constructor(
    private store: Store<AppState>,
    private productActions: ProductActions,
    private route: ActivatedRoute,
    private router: Router
    ) { 
  }

  ngOnInit() {
    // Subscribe for changes on the product state.
    let productObservable: Observable<ProductState> = this.store.select('product');
    this.productSubscription = productObservable.subscribe(
      (next) => { 
        this.product = next.product;
        this.loading = next.loading;
        this.saving = next.saving;
      }
    );

    // Load the product according to the id parameter.
    this.route.params.subscribe(params => {
      if(params.id) {
        this.store.dispatch(this.productActions.getProduct(params.id));
      } else {
        this.store.dispatch(this.productActions.resetBlankProduct());
      }
    });
  }

  ngOnDestroy() {
    this.productSubscription.unsubscribe();
  }

  save(form: any, product: Product) {
    this.store.dispatch(this.productActions.saveProduct(product));
  }

  delete(product: Product) {
    if(confirm("Are you sure you want to delete this product?")) {
      this.store.dispatch(this.productActions.deleteProduct(product));
      this.router.navigate(['products']);
    }
  }
}