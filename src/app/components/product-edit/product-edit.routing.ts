import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductEditComponent } from './product-edit.component';

const routes: Routes = [
  { path: '', component: ProductEditComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);