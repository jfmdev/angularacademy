import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { ProductEditComponent } from './product-edit.component';
import { routing } from './product-edit.routing';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      routing
  ],
  declarations: [ProductEditComponent]
})
export class ProductEditModule {}