import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { User } from '../../models/user';
import { Product } from '../../models/product';
import { ProductActions } from '../../actions/product';
import { AppState } from '../../reducers/app-state'
import { ProductListState } from '../../reducers/product-list';
import { SessionState } from '../../reducers/session';
import { ActiveUserService } from '../../services/active-user';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html'
})

export class ProductListComponent implements OnInit {
  products: Observable<ProductListState>;

  constructor(
    private store: Store<AppState>,
    private productActions: ProductActions,
    private activeUser: ActiveUserService
    ) { 
  }

  ngOnInit() {
    this.products = this.store.select('productList');
    this.store.dispatch(this.productActions.loadProducts());
  }
  
  delete(product: Product) {
    if(confirm("Are you sure you want to delete product " + product.name)) {
      this.store.dispatch(this.productActions.deleteProduct(product));
    }
    return false;
  }
}