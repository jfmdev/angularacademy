import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { RecaptchaModule } from 'ng2-recaptcha';

import { EqualValidator } from '../../shared/equal-validator.directive'; 

import { RegistrationComponent } from './registration.component';
import { routing } from './registration.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RecaptchaModule.forRoot(),
    routing
  ],
  declarations: [
    EqualValidator,
    RegistrationComponent
  ]
})
export class RegistrationModule {}