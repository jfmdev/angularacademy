import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { User } from '../../models/user';
import { UserActions } from '../../actions/user';
import { SessionActions } from '../../actions/session';
import { AppState } from '../../reducers/app-state'
import { UserState } from '../../reducers/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})

export class RegistrationComponent implements OnInit  {
  userSubscription: any;
  user: User;
  passwordConfirm: string;
  saving: boolean;
  captcha: boolean;

  constructor(
    private store: Store<AppState>,
    private userActions: UserActions,
    private sessionActions: SessionActions,
    private route: ActivatedRoute,
    private router: Router
    ) {
  }

  ngOnInit() {
    // Initialize variables.
    this.captcha = false;
    this.saving = false;
    this.passwordConfirm = '';
    
    // Subscribe for changes on the user state.
    let userObservable: Observable<UserState> = this.store.select('user');
    this.userSubscription = userObservable.subscribe(
      (next) => { 
        this.user = next.user;
        this.saving = next.saving;
        if(next.user.id) {
          this.store.dispatch(this.sessionActions.loginSuccess(next));
          this.router.navigate(['orders']);
        }
      }
    );

    // Dispatch action for reset the user.
    this.store.dispatch(this.userActions.resetBlankUser());
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  save(form: any, user: User) {
    user.roles = ['user'];
    this.store.dispatch(this.userActions.saveUser(user));
  }

  captchaResolved(captchaResponse: string) {
    this.captcha = true;
  }
}