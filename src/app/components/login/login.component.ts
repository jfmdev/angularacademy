import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { SessionActions } from '../../actions/session';
import { AppState } from '../../reducers/app-state'
import { SessionState } from '../../reducers/session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy  {
  sessionSubscription: any;
  account: any;
  loading: boolean;
  error: string;

  constructor(
    private store: Store<AppState>,
    private sessionActions: SessionActions,
    private route: ActivatedRoute,
    private router: Router
    ) {
  }

  ngOnInit() {
    // Initialize variables.
    this.account = {
      username: '',
      password: ''
    };
    
    // Listen for changes on the session.
    let sessionObservable: Observable<SessionState> = this.store.select('session');
    this.sessionSubscription = sessionObservable.subscribe(
      (next) => { 
        this.loading = next.loading;
        this.error = next.error;
        if(next.user) {
          this.router.navigate(['orders']);
        }
      }
    );
  }

  ngOnDestroy() {
    this.sessionSubscription.unsubscribe();
  }
  
  login(form: any, account: any) {
    this.store.dispatch(this.sessionActions.login(account.username, account.password));
  }
}
