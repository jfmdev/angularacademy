import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { OrderEditComponent } from './order-edit.component';
import { routing } from './order-edit.routing';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      routing
  ],
  declarations: [OrderEditComponent]
})
export class OrderEditModule {}