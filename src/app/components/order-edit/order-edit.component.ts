import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';

import { ActiveUserService } from '../../services/active-user';
import { Product } from '../../models/product';
import { ProductActions } from '../../actions/product';
import { ProductListState } from '../../reducers/product-list';
import { Order } from '../../models/order';
import { OrderLine } from '../../models/order-line';
import { OrderActions } from '../../actions/order';
import { OrderState } from '../../reducers/order';
import { AppState } from '../../reducers/app-state'

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html'
})

export class OrderEditComponent implements OnInit, OnDestroy  {
  orderSubscription: any;
  productsSubscription: any;
  order: Order;
  products: Product[];
  saving: boolean;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private productActions: ProductActions,
    private orderActions: OrderActions,
    private activeUser: ActiveUserService
    ) { 
  }

  ngOnInit() {
    // Subscribe for changes on the order state.
    let orderObservable: Observable<OrderState> = this.store.select('order');
    this.orderSubscription = orderObservable.subscribe(
      (next) => { 
        this.order = next.order;
        this.order.lines = this.order.lines || [];
        this.saving = next.saving;
        if(this.order.lines.length == 0) {
          this.addLine(this.order);
        }
      }
    );

    // Create blank order.
    this.store.dispatch(this.orderActions.resetBlankOrder());
    
    // Subscribe for changes on the products state.
    let productsObservable: Observable<ProductListState> = this.store.select('productList');
    this.productsSubscription = productsObservable.subscribe(
      (next) => { 
        this.products = next;
      }
    );

    // Load products list.
    this.store.dispatch(this.productActions.loadProducts());
    
  }

  ngOnDestroy() {
    this.orderSubscription.unsubscribe();
    this.productsSubscription.unsubscribe();
  }

  save(form: any, order: Order) {
    order.userId = this.activeUser.user.id;
    order.date = new Date();
    order.status = 'created';
    this.store.dispatch(this.orderActions.saveOrder(order));
  }
  
  addLine(order: Order) {
    order.lines.push({'productId': null, 'quantity': 0});
    return false;
  }
  
  removeLine(order: Order, orderLine: OrderLine) {
    order.lines = _.without(order.lines, orderLine);
    return false;
  }
}
