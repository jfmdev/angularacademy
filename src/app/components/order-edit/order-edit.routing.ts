import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderEditComponent } from './order-edit.component';

const routes: Routes = [
  { path: '', component: OrderEditComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);