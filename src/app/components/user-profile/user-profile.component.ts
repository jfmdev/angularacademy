import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { User } from '../../models/user';
import { SessionActions } from '../../actions/session';
import { SessionState } from '../../reducers/session';
import { AppState } from '../../reducers/app-state'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html'
})

export class UserProfileComponent implements OnInit, OnDestroy  {
  sessionSubscription: any;
  user: User;
  loading: boolean;
  saving: boolean;

  constructor(
    private store: Store<AppState>,
    private sessionActions: SessionActions,
    private route: ActivatedRoute,
    private router: Router
    ) { 
  }

  ngOnInit() {
    // Subscribe for changes on the session.
    let sessionObservable: Observable<SessionState> = this.store.select('session');
    this.sessionSubscription = sessionObservable.subscribe(
      (next) => { 
        this.loading = next.loading;
        this.saving = next.saving;
        this.user = next.user;
      }
    );
  }

  ngOnDestroy() {
    this.sessionSubscription.unsubscribe();
  }

  save(form: any, user: User) {
    this.store.dispatch(this.sessionActions.updateProfile(user));
  }
}