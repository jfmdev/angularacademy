import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { UserProfileComponent } from './user-profile.component';
import { routing } from './user-profile.routing';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      routing
  ],
  declarations: [UserProfileComponent]
})
export class UserProfileModule {}