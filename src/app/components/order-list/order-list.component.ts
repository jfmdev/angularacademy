import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';

import { User } from '../../models/user';
import { Order } from '../../models/order';
import { OrderActions } from '../../actions/order';
import { AppState } from '../../reducers/app-state'
import { OrderListState } from '../../reducers/order-list';
import { SessionState } from '../../reducers/session';
import { ActiveUserService } from '../../services/active-user';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html'
})

export class OrderListComponent implements OnInit {
  orders: Observable<OrderListState>;

  constructor(
    private store: Store<AppState>,
    private orderActions: OrderActions,
    private activeUser: ActiveUserService
    ) { 
  }

  ngOnInit() {
    this.orders = this.store.select('orderList');
    this.store.dispatch(this.orderActions.loadOrders());
  }
  
  delete(order: Order) {
    if(confirm("Are you sure you want to delete the order " + order.id)) {
      this.store.dispatch(this.orderActions.deleteOrder(order));
    }
    return false;
  }
}
