import { Component, OnInit, OnDestroy } from '@angular/core';
import { RouterOutlet, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { AppState } from './reducers/app-state'
import { SessionActions } from './actions/session';
import { SessionState } from './reducers/session';
import { ActiveUserService } from './services/active-user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: []
})

export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private store: Store<AppState>,
    private sessionActions: SessionActions,
    private route: ActivatedRoute,
    private router: Router,
    private activeUser: ActiveUserService
  ) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  logout() {
    this.store.dispatch(this.sessionActions.logout());
    this.router.navigate(['login']);
    return false;
  }
}