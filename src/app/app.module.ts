import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { OrderListComponent } from './components/order-list/order-list.component';
import { OrderViewComponent } from './components/order-view/order-view.component';

import { AppRoutingModule } from './app-routing.module';

import { FirebaseService } from './services/firebase';
import { UserService } from './services/user';
import { SessionService } from './services/session';
import { ProductService } from './services/product';
import { RoleService } from './services/role';
import { ActiveUserService } from './services/active-user';
import { StatusService } from './services/status';
import { OrderService } from './services/order';

import { UserActions } from './actions/user';
import { SessionActions } from './actions/session';
import { ProductActions } from './actions/product';
import { OrderActions } from './actions/order';

import { UserEffects } from './effects/user';
import { SessionEffects } from './effects/session';
import { ProductEffects } from './effects/product';
import { OrderEffects } from './effects/order';

import { UserListReducer } from './reducers/user-list';
import { UserReducer } from './reducers/user';
import { SessionReducer } from './reducers/session';
import { ProductListReducer } from './reducers/product-list';
import { ProductReducer } from './reducers/product';
import { OrderListReducer } from './reducers/order-list';
import { OrderReducer } from './reducers/order';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OrderListComponent,
    OrderViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    StoreModule.forRoot({ 
      user: UserReducer, 
      userList: UserListReducer,
      session: SessionReducer,
      product: ProductReducer, 
      productList: ProductListReducer,
      order: OrderReducer, 
      orderList: OrderListReducer
    }),
    EffectsModule.forRoot([
      UserEffects,
      SessionEffects,
      ProductEffects,
      OrderEffects
    ])
  ],
  providers: [
    FirebaseService,
    UserService,
    RoleService,
    SessionService,
    ProductService,
    ActiveUserService,
    StatusService,
    OrderService,
    UserActions,
    SessionActions,
    ProductActions,
    OrderActions
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }