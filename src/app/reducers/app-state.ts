import { UserState } from './user';
import { UserListState } from './user-list';
import { SessionState } from './session';
import { ProductState } from './product';
import { ProductListState } from './product-list';
import { OrderState } from './order';
import { OrderListState } from './order-list';

export interface AppState {
    session: SessionState;
    user: UserState;
    userList: UserListState;
    product: ProductState;
    productList: ProductListState;
    order: OrderState;
    orderList: OrderListState;
};