import {Action} from '@ngrx/store';

import {Order} from '../models/order';
import {OrderActions} from '../actions/order';
import {UnsafeAction} from '../actions/unsafeAction';
import * as _ from 'lodash';

export interface OrderState {
  order: Order;
  loading: boolean;
  saving: boolean;
};

const initialState: OrderState = {
  order: {
    id: '',
    status: null,
    userId: null,
    lines: [],
    date: null
  },
  loading: false,
  saving: false
};

export function OrderReducer(state = initialState, action: UnsafeAction): OrderState {
  switch (action.type) {
    case OrderActions.RESET_BLANK_ORDER: {
      return _.assign({}, state, {
        order: _.cloneDeep(initialState),
        loading: false,
        saving: false
      });
    }

    case OrderActions.GET_ORDER: {
      return _.assign({}, state, {
        loading: true
      });
    }

    case OrderActions.ADD_ORDER:
    case OrderActions.SAVE_ORDER: {
      return _.assign({}, state, {
        saving: true
      });
    }

    case OrderActions.GET_ORDER_SUCCESS: {
      return _.assign({}, state, {
        order: action.payload,
        loading: false,
        saving: false
      });
    }

    case OrderActions.SAVE_ORDER_SUCCESS: {
      return _.assign({}, state, {
        order: action.payload,
        loading: false,
        saving: false
      });
    }

    default: {
      return state;
    }
  }
}