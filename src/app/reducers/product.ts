import {Action} from '@ngrx/store';

import {Product} from '../models/product';
import {ProductActions} from '../actions/product';
import {UnsafeAction} from '../actions/unsafeAction';
import * as _ from 'lodash';

export interface ProductState {
  product: Product;
  loading: boolean;
  saving: boolean;
};

const initialState: ProductState = {
  product: {
    id: '',
    name: '',
    description: '',
    price: 0
  },
  loading: false,
  saving: false
};

export function ProductReducer(state = initialState, action: UnsafeAction): ProductState {
  switch (action.type) {
    case ProductActions.RESET_BLANK_PRODUCT: {
      return _.assign({}, state, {
        product: _.cloneDeep(initialState),
        loading: false,
        saving: false
      });
    }

    case ProductActions.GET_PRODUCT: {
      return _.assign({}, state, {
        loading: true
      });
    }

    case ProductActions.ADD_PRODUCT:
    case ProductActions.SAVE_PRODUCT: {
      return _.assign({}, state, {
        saving: true
      });
    }

    case ProductActions.GET_PRODUCT_SUCCESS: {
      return _.assign({}, state, {
        product: action.payload,
        loading: false,
        saving: false
      });
    }

    case ProductActions.SAVE_PRODUCT_SUCCESS: {
      return _.assign({}, state, {
        product: action.payload,
        loading: false,
        saving: false
      });
    }

    default: {
      return state;
    }
  }
}