import {Action} from '@ngrx/store';

import {User} from '../models/user';
import {SessionActions} from '../actions/session';
import {UnsafeAction} from '../actions/unsafeAction';
import * as _ from 'lodash';

export interface SessionState {
  user?: User;
  error?: string;
  loading: boolean;
  saving: boolean;
};

const initialState: SessionState = {
  user: null,
  error: null,
  loading: false,
  saving: false
};

export function SessionReducer(state = initialState, action: UnsafeAction): SessionState {
  switch (action.type) {
    case SessionActions.LOGOUT: {
      return _.clone(initialState);
    }
    
    case SessionActions.LOGIN: {
      return _.assign({}, state, {
        loading: true
      });
    }
      
    case SessionActions.LOGIN_SUCCESS: {
      return _.assign({}, state, {
        user: action.payload,
        error: null,
        loading: false
      });
    }
    
    case SessionActions.LOGIN_ERROR: {
      return _.assign({}, state, {
        user: null,
        error: action.payload,
        loading: false
      });
    }

    case SessionActions.UPDATE_PROFILE: {
      return _.assign({}, state, {
        saving: true
      });
    }

    case SessionActions.UPDATE_PROFILE_SUCCESS: {
      return _.assign({}, state, {
        user: action.payload,
        loading: false,
        saving: false
      });
    }

    default: {
      return state;
    }
  }
}