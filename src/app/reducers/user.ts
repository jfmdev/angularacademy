import {Action} from '@ngrx/store';

import {User} from '../models/user';
import {UserActions} from '../actions/user';
import {UnsafeAction} from '../actions/unsafeAction';
import * as _ from 'lodash';

export interface UserState {
  user: User;
  loading: boolean;
  saving: boolean;
};

const initialState: UserState = {
  user: {
    id: '',
    username: '',
    password: '',
    full_name: '',
    billing_address: '',
    shipping_address: '',
    roles: []
  },
  loading: false,
  saving: false
};

export function UserReducer(state = initialState, action: UnsafeAction): UserState {
  switch (action.type) {
    case UserActions.RESET_BLANK_USER: {
      return _.assign({}, state, {
        user: _.cloneDeep(initialState),
        loading: false,
        saving: false
      });
    }

    case UserActions.GET_USER: {
      return _.assign({}, state, {
        loading: true
      });
    }

    case UserActions.ADD_USER:
    case UserActions.SAVE_USER: {
      return _.assign({}, state, {
        saving: true
      });
    }

    case UserActions.GET_USER_SUCCESS: {
      return _.assign({}, state, {
        user: action.payload,
        loading: false,
        saving: false
      });
    }

    case UserActions.SAVE_USER_SUCCESS: {
      return _.assign({}, state, {
        user: action.payload,
        loading: false,
        saving: false
      });
    }

    default: {
      return state;
    }
  }
}