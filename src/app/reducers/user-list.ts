import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {User} from '../models/user';
import {UnsafeAction} from '../actions/unsafeAction';
import {UserActions} from '../actions/user';
import * as _ from 'lodash';

// TODO: the state should include a loading flag.
export type UserListState = User[];

const initialState: UserListState = [];

export function UserListReducer(state = initialState, action: UnsafeAction): UserListState {
  switch (action.type) {
    case UserActions.LOAD_USERS_SUCCESS: {
      return action.payload;
    }
    case UserActions.ADD_USER_SUCCESS: {
      return [...state, action.payload];
    }
    case UserActions.SAVE_USER_SUCCESS: {
      let index = _.findIndex(state, {id: action.payload.id});
      if (index >= 0) {
        return [
          ...state.slice(0, index),
          action.payload,
          ...state.slice(index + 1)
        ];
      }
      return state;
    }
    case UserActions.DELETE_USER_SUCCESS: {
      return state.filter(user => {
        return user.id !== action.payload.id;
      });
    }
    default: {
      return state;
    }
  }
}