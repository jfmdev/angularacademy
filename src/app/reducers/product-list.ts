import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {Product} from '../models/product';
import {UnsafeAction} from '../actions/unsafeAction';
import {ProductActions} from '../actions/product';
import * as _ from 'lodash';

// TODO: the state should include a loading flag.
export type ProductListState = Product[];

const initialState: ProductListState = [];

export function ProductListReducer(state = initialState, action: UnsafeAction): ProductListState {
  switch (action.type) {
    case ProductActions.LOAD_PRODUCTS_SUCCESS: {
      return action.payload;
    }
    case ProductActions.ADD_PRODUCT_SUCCESS: {
      return [...state, action.payload];
    }
    case ProductActions.SAVE_PRODUCT_SUCCESS: {
      let index = _.findIndex(state, {id: action.payload.id});
      if (index >= 0) {
        return [
          ...state.slice(0, index),
          action.payload,
          ...state.slice(index + 1)
        ];
      }
      return state;
    }
    case ProductActions.DELETE_PRODUCT_SUCCESS: {
      return state.filter(product => {
        return product.id !== action.payload.id;
      });
    }
    default: {
      return state;
    }
  }
}