import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {Order} from '../models/order';
import {UnsafeAction} from '../actions/unsafeAction';
import {OrderActions} from '../actions/order';
import * as _ from 'lodash';

// TODO: the state should include a loading flag.
export type OrderListState = Order[];

const initialState: OrderListState = [];

export function OrderListReducer(state = initialState, action: UnsafeAction): OrderListState {
  switch (action.type) {
    case OrderActions.LOAD_ORDERS_SUCCESS: {
      return action.payload;
    }
    case OrderActions.ADD_ORDER_SUCCESS: {
      return [...state, action.payload];
    }
    case OrderActions.SAVE_ORDER_SUCCESS: {
      let index = _.findIndex(state, {id: action.payload.id});
      if (index >= 0) {
        return [
          ...state.slice(0, index),
          action.payload,
          ...state.slice(index + 1)
        ];
      }
      return state;
    }
    case OrderActions.DELETE_ORDER_SUCCESS: {
      return state.filter(order => {
        return order.id !== action.payload.id;
      });
    }
    default: {
      return state;
    }
  }
}