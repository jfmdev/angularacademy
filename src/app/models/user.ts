export interface User {
  id: string;
  username: string;
  password: string;
  full_name: string;
  billing_address: string;
  shipping_address: string;
  roles: string[];
}
