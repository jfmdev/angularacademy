import {OrderLine} from './order-line';

export interface Order {
  id: string;
  userId?: string;
  lines: OrderLine[];
  date?: Date;
  status?: string;
}
