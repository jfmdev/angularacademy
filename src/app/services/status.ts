import { Injectable } from '@angular/core';

import { Status } from '../models/status'

@Injectable()
export class StatusService {
  getOrderStatuses() : Status[] {
    return [
      {key: 'created', name: 'Created'},
      {key: 'payed', name: 'Payed'},
      {key: 'delivered', name: 'Delivered'},
      {key: 'cancelled', name: 'Cancelled'}
    ];
  }
}