import {Injectable, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';

import {AppState} from '../reducers/app-state'
import {UnsafeAction} from '../actions/unsafeAction';
import {SessionService} from '../services/session';
import {SessionState} from '../reducers/session';
import {User} from '../models/user';

@Injectable()
export class ActiveUserService implements OnDestroy {
  sessionSubscription: any;
  user: User;
  
  constructor (
    private store: Store<AppState>,
    private sessionSvc: SessionService
  ) {
    this.user = null;
    
    let sessionObservable: Observable<SessionState> = this.store.select('session');
    this.sessionSubscription = sessionObservable.subscribe(
      (next) => { 
        this.user = next.user;
      }
    );
  }

  ngOnDestroy() {
    this.sessionSubscription.unsubscribe();
  }

  hasRole(role: string) {
    return this.user && this.user.roles.indexOf(role) >= 0;
  }
  
  isLogged() {
    return this.user != null && this.user.id != null;
  }
  
  getId() {
    return this.user? this.user.id : null;
  }
}