import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Product } from '../models/product'
import { FirebaseService } from './firebase'

@Injectable()
export class ProductService extends FirebaseService {  
    getProducts(): Observable<Product[]> {
        return this.get(FirebaseService.Collections.Product).map(res => res as Product[]);
    }

    getProduct(id: string): Observable<Product> {
        return this.getById(FirebaseService.Collections.Product, id).map(res => res as Product);
    }

    saveProduct(product: Product): Observable<string> {
        return this.save(FirebaseService.Collections.Product, product);
    }

    updateProduct(product: Product): Observable<string> {
        return this.update(FirebaseService.Collections.Product, product);
    }

    addProduct(product: Product): Observable<string> {
        return this.add(FirebaseService.Collections.Product, product);
    }

    deleteProduct(id: string): Observable<Product> {
        return this.delete(FirebaseService.Collections.Product, id);
    }
}