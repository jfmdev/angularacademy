import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { User } from '../models/user'
import { FirebaseService } from './firebase'

@Injectable()
export class UserService extends FirebaseService {  
    getUsers(): Observable<User[]> {
        return this.get(FirebaseService.Collections.User).map(res => res as User[]);
    }

    getUser(id: string): Observable<User> {
        return this.getById(FirebaseService.Collections.User, id).map(res => res as User);
    }

    saveUser(user: User): Observable<string> {
        return this.save(FirebaseService.Collections.User, user);
    }

    updateUser(user: User): Observable<string> {
        return this.update(FirebaseService.Collections.User, user);
    }

    addUser(user: User): Observable<string> {
        return this.add(FirebaseService.Collections.User, user);
    }

    deleteUser(id: string): Observable<User> {
        return this.delete(FirebaseService.Collections.User, id);
    }
}