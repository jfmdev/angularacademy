import { Injectable } from '@angular/core';

import { Role } from '../models/role'

@Injectable()
export class RoleService {
  getList() : Role[] {
    return [
      {key: 'user', name: 'User'},
      {key: 'manager', name: 'Manager'},
      {key: 'admin', name: 'Admin'}
    ];
  }
}