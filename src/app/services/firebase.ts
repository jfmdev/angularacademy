import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import * as _ from "lodash";

@Injectable()
export class FirebaseService {
    // --- Constants --- //
  
    static Collections = {
        User: 'user',
        Product: 'product',
        Order: 'order'
    };
  
    // TODO: read these values (or at least the ProjectID) from a configuration file.
    private baseUrl = "https://angular-e7e0f.firebaseio.com/{COLLECTION}.json";
    private baseItemUrl = "https://angular-e7e0f.firebaseio.com/{COLLECTION}/{ID}.json";
  
    // --- Constructor --- //
  
    constructor(private http: Http) {}

    // --- Main methods --- //
    
    get(collection: string): Observable<any[]> {
        return this.http.get(this.baseUrl.replace('{COLLECTION}', collection))
        .map(res => _.map(res.json(), (item, key) => _.assign(item, {'id': key})));
    }

    getById(collection: string, id: string): Observable<any> {
        return this.http.get(this.baseItemUrl.replace('{COLLECTION}', collection).replace('{ID}', id))
        .map(res => _.assign(res.json(), {'id': id}));
    }

    save(collection: string, data:any): Observable<string> {
        if (data.id) {
            return this.update(collection, data);
        } else {
            return this.add(collection, data);
        }
    }

    update(collection: string, data:any): Observable<string> {
        return this.http.put(this.baseItemUrl.replace('{COLLECTION}', collection).replace('{ID}', data.id), data)
        .map(res => res.json().id);
    }

    add(collection: string, data:any): Observable<string> {
        return this.http.post(this.baseUrl.replace('{COLLECTION}', collection), data)
        .map(res => res.json().name);
    }

    delete(collection: string, id: string): Observable<any> {
        return this.http.delete(this.baseItemUrl.replace('{COLLECTION}', collection).replace('{ID}', id))
        .map(res => ({'id': id})); // Firebase returns 'null' when deleting a document.
    }
}