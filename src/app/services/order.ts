import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as _ from 'lodash';

import { Order } from '../models/order'
import { FirebaseService } from './firebase'

@Injectable()
export class OrderService extends FirebaseService {  
  getOrders(): Observable<Order[]> {
    return this.get(FirebaseService.Collections.Order).map(res => res as Order[]);
  }

  getOrder(id: string): Observable<Order> {
    return this.getById(FirebaseService.Collections.Order, id).map(res => res as Order);
  }

  saveOrder(order: Order): Observable<string> {
    return this.save(FirebaseService.Collections.Order, order);
  }

  updateOrder(order: Order): Observable<string> {
    return this.update(FirebaseService.Collections.Order, order);
  }

  addOrder(order: Order): Observable<string> {
    return this.add(FirebaseService.Collections.Order, order);
  }

  deleteOrder(id: string): Observable<Order> {
    return this.delete(FirebaseService.Collections.Order, id);
  }
}