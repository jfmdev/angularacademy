import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as _ from 'lodash';

import { User } from '../models/user'
import { UserService } from './user'

@Injectable()
export class SessionService extends UserService {
  // HACK: For login, I'm just getting the list of users and checking if there is a match.
  login(username: string, password: string): Observable<User> {
    return this.getUsers().map(users => {
      let filtered = _.filter(users, user => (user.username == username && user.password == password));
      return filtered.length > 0? filtered[0] as User : null;
    });
  }
}