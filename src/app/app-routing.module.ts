import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { LoginComponent } from './components/login/login.component';
import { OrderListComponent } from './components/order-list/order-list.component';
import { OrderViewComponent } from './components/order-view/order-view.component';
 
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', loadChildren: './components/registration/registration.module#RegistrationModule' },
  { path: 'user', loadChildren: './components/user/user.module#UserModule' },
  { path: 'orders', component: OrderListComponent },
  { path: 'order', loadChildren: './components/order-edit/order-edit.module#OrderEditModule' },
  { path: 'order/:id', component: OrderViewComponent },
  { path: 'products', loadChildren: './components/product-list/product-list.module#ProductListModule' },
  { path: 'product', loadChildren: './components/product-edit/product-edit.module#ProductEditModule' },
  { path: 'product/:id', loadChildren: './components/product-edit/product-edit.module#ProductEditModule' },
  { path: 'profile', loadChildren: './components/user-profile/user-profile.module#UserProfileModule' },
  { path: 'checkout/:orderId', loadChildren: './components/checkout/checkout.module#CheckoutModule' }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}