import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Effect, Actions} from '@ngrx/effects';
import {Action} from '@ngrx/store';

import {ProductActions} from '../actions/product';
import {UnsafeAction} from '../actions/unsafeAction';
import {ProductService} from '../services/product';

@Injectable()
export class ProductEffects {
  constructor (
    private actions$: Actions,
    private productActions: ProductActions,
    private svc: ProductService,
  ) {}

  @Effect() loadProducts$: Observable<Action> = this.actions$
    .ofType(ProductActions.LOAD_PRODUCTS)
    .mergeMap(() => this.svc.getProducts())
    .map(products => this.productActions.loadProductsSuccess(products));

  @Effect() getProduct$: Observable<Action> = this.actions$
    .ofType(ProductActions.GET_PRODUCT)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(id => this.svc.getProduct(id))
    .map(product => this.productActions.getProductSuccess(product));

  @Effect() saveProduct$: Observable<Action> = this.actions$
    .ofType(ProductActions.SAVE_PRODUCT)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(product => this.svc.saveProduct(product))
    .mergeMap(productId => this.svc.getProduct(productId))
    .map(product => this.productActions.saveProductSuccess(product));

  @Effect() addProduct$: Observable<Action> = this.actions$
    .ofType(ProductActions.ADD_PRODUCT)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(product => this.svc.saveProduct(product))
    .mergeMap(productId => this.svc.getProduct(productId))
    .map(product => this.productActions.addProductSuccess(product));

  @Effect() deleteProduct$: Observable<Action> = this.actions$
    .ofType(ProductActions.DELETE_PRODUCT)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(product => this.svc.deleteProduct(product.id))
    .map(product => this.productActions.deleteProductSuccess(product));
}
