import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Effect, Actions} from '@ngrx/effects';
import {Action} from '@ngrx/store';

import {OrderActions} from '../actions/order';
import {UnsafeAction} from '../actions/unsafeAction';
import {OrderService} from '../services/order';

@Injectable()
export class OrderEffects {
  constructor (
    private actions$: Actions,
    private orderActions: OrderActions,
    private svc: OrderService,
  ) {}

  @Effect() loadOrders$: Observable<Action> = this.actions$
    .ofType(OrderActions.LOAD_ORDERS)
    .mergeMap(() => this.svc.getOrders())
    .map(orders => this.orderActions.loadOrdersSuccess(orders));

  @Effect() getOrder$: Observable<Action> = this.actions$
    .ofType(OrderActions.GET_ORDER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(id => this.svc.getOrder(id))
    .map(order => this.orderActions.getOrderSuccess(order));

  @Effect() saveOrder$: Observable<Action> = this.actions$
    .ofType(OrderActions.SAVE_ORDER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(order => this.svc.saveOrder(order))
    .mergeMap(orderId => this.svc.getOrder(orderId))
    .map(order => this.orderActions.saveOrderSuccess(order));

  @Effect() addOrder$: Observable<Action> = this.actions$
    .ofType(OrderActions.ADD_ORDER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(order => this.svc.saveOrder(order))
    .mergeMap(orderId => this.svc.getOrder(orderId))
    .map(order => this.orderActions.addOrderSuccess(order));

  @Effect() deleteOrder$: Observable<Action> = this.actions$
    .ofType(OrderActions.DELETE_ORDER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(order => this.svc.deleteOrder(order.id))
    .map(order => this.orderActions.deleteOrderSuccess(order));
}
