import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Effect, Actions} from '@ngrx/effects';
import {Action} from '@ngrx/store';

import {UnsafeAction} from '../actions/unsafeAction';
import {SessionActions} from '../actions/session';
import {SessionService} from '../services/session';
import {UserService} from '../services/user';

@Injectable()
export class SessionEffects {
  constructor (
    private actions$: Actions,
    private sessionActions: SessionActions,
    private sessionSvc: SessionService,
    private userSvc: UserService
  ) {}

  @Effect() login$: Observable<Action> = this.actions$
    .ofType(SessionActions.LOGIN)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(credentials => this.sessionSvc.login(credentials.username, credentials.password))
    .map(user => {
      if(user) {
        return this.sessionActions.loginSuccess(user);
      } else {
        return this.sessionActions.loginError("Invalid username or password");
      }
    });

  @Effect() updateProfile$: Observable<Action> = this.actions$
    .ofType(SessionActions.UPDATE_PROFILE)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(user => this.userSvc.saveUser(user))
    .mergeMap(userId => this.userSvc.getUser(userId))
    .map(user => this.sessionActions.updateProfileSuccess(user));
}
