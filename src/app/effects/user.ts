import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Effect, Actions} from '@ngrx/effects';
import {Action} from '@ngrx/store';

import {UserActions} from '../actions/user';
import {UnsafeAction} from '../actions/unsafeAction';
import {UserService} from '../services/user';

@Injectable()
export class UserEffects {
  constructor (
    private actions$: Actions,
    private userActions: UserActions,
    private svc: UserService,
  ) {}

  @Effect() loadUsers$: Observable<Action> = this.actions$
    .ofType(UserActions.LOAD_USERS)
    .mergeMap(() => this.svc.getUsers())
    .map(users => this.userActions.loadUsersSuccess(users));

  @Effect() getUser$: Observable<Action> = this.actions$
    .ofType(UserActions.GET_USER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(id => this.svc.getUser(id))
    .map(user => this.userActions.getUserSuccess(user));

  @Effect() saveUser$: Observable<Action> = this.actions$
    .ofType(UserActions.SAVE_USER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(user => this.svc.saveUser(user))
    .mergeMap(userId => this.svc.getUser(userId))
    .map(user => this.userActions.saveUserSuccess(user));

  @Effect() addUser$: Observable<Action> = this.actions$
    .ofType(UserActions.ADD_USER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(user => this.svc.saveUser(user))
    .mergeMap(userId => this.svc.getUser(userId))
    .map(user => this.userActions.addUserSuccess(user));

  @Effect() deleteUser$: Observable<Action> = this.actions$
    .ofType(UserActions.DELETE_USER)
    .map((action: UnsafeAction) => action.payload)
    .mergeMap(user => this.svc.deleteUser(user.id))
    .map(user => this.userActions.deleteUserSuccess(user));
}
